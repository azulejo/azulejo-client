import babel from 'rollup-plugin-babel'
import postcss from 'rollup-plugin-postcss'

export default [{
  input: 'src/App.js',
  output:{
    format: "umd",
    name: 'App',
  },
  plugins: [
    postcss({
      extract: true,
      plugins: []
    }),
    babel(),
  ]
}]
